/* vim: set filetype=groovy : */
// Declarative syntax Jenkinsfile for running unit tests, style checks, build image and store artifact
//
/* --------------------
    App specific vars
   -------------------- */
def APP_VERSION = 'NOT_SET_JENKINSFILE' // This is set later from package.json.
//def HELM_CHART = 'helm-act-template'
//def HELM_CHART_VERSION = '0.5.0'
////If integration code is housed in a different repository
//def INTEGRATION_REPOSITORY
//def INTEGRATION_SMOKE_FILTER = 'TestCategory=SmokeTests'
//def INTEGRATION_RESULTS_PATH = '/app/ACT.Automation.Template.Tests/TestResults/*.trx'
//def INTEGRATION_SOLUTION_PATH = './integration-tests/ACT.Automation.Template.Tests.sln'
//def INTEGRATION_BINARY = './integration-tests/ACT.Automation.Template.Tests/bin/Release/netcoreapp2.2/ACT.Automation.Template.Tests.dll'

/* ---------------------
   Build specific vars
   --------------------- */
// Bitbucket and Github use different default 'master' branches
def MASTER_BRANCH_NAME = 'DEV'
def AF_REGISTRY = 'docker-jenkins.artifactory.dev.actsw.net'
def AF_MAX_BUILDS = 10                       // The maximum number of builds to keep in artifactory
def AF_MAX_MASTER_BUILDS = 30                // The maximum number of builds of the master branch to keep in artifactory
def BSI_SECRET_ID = 'FOD-BSI-or-command'     // The Jenkins secret id of the Fortify BSI token for this project
def API_SECRET_ID = 'FOD-Jenkins-API-key'    // The Jenkins secret id of the Fortify API key and secret for this project
def SL_SECRET_ID  = 'SL-Jenkins-key'         // The Jenkins secret id of the Sealights API token and secret for this project
def CODESIGN_P12  = 'CodeSign-PKCS12'        // The Jenkins certificate for DigiCert Code Signing PKCS#12 Certificate
def K8S_IMAGE_PULL_SECRET = 'docker-jenkins.artifactory.dev.actsw.net' // The k8s secret required to pull images from Artifactory
def K8S_POD_SERVICE_ACCOUNT = 'helm-admin' // The k8s service account that has enough privileges to deploy helm charts

/* ---------------------
   Global Build vars
   --------------------- */
def RUN_SECURITY_SCAN = true // This is a build-wide flag to run the security scan.  It should be toggled by either parameter or the .fodignore search
def GIT_BRANCH
def GIT_COMMIT
def GIT_PREVIOUS_COMMIT
def IMAGE_NAME
def REGISTRY_URI
/* Define default step time here since the first run of a branch/PR does not pick up the default value from the parameter.
 https://issues.jenkins-ci.org/browse/JENKINS-41929 */
def STEP_TIME_LIMIT = '20'
def BUILD_TIMEOUT = '30'
def ProductVersion = 'NONE'

pipeline {

  parameters {
    booleanParam(name: 'FULL_INTEGRATION_TESTS', defaultValue: false, description: 'Whether or not to run all integration tests.')
    booleanParam(name: 'SECURITY_SCAN', defaultValue: false, description: 'Whether or not to submit code to Fortify On Demand')
    booleanParam(name: 'KEEP_INTEGRATION_ENVIRONMENT', defaultValue: false, description: 'Whether or not to keep the integration environment')
    string(name: 'TIME_LIMIT', defaultValue: '20', description: 'Maximum allowed time for each of the security and integration stages')
    string(name: 'PRODUCT_VERSION', defaultValue: 'NONE', description: 'Version to bake into the MSI. If left "NONE" it defaults to 0.0.BUILD_NUMBER')
  }

  options {
    // FOD does not like concurrent builds, and will error if you submit one while running.
    disableConcurrentBuilds()
    // Keep the last 180 days of builds.
    buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '5', daysToKeepStr: '180', numToKeepStr: '')
    timeout(BUILD_TIMEOUT)
  }

  agent none

  stages {
    stage('Kubernetes Stages Parent') {
      agent {
        kubernetes {
          defaultContainer 'node'
          yamlFile 'pods/default.yaml'
        }
      }

      stages {
        stage('Check OS & tools') {
          steps {
            sh 'cat /etc/os-release'
            // This is needed if the `npm ci` process needs to access private repos.
            sh 'mkdir -p -m 0700 ~/.ssh'
            sh 'ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts'
            sh 'ssh-keyscan github.com >> ~/.ssh/known_hosts'
            sh 'cat ~/.ssh/known_hosts'
            sh 'echo $HOME'
            script {
              // Set the product version for the MSI
              if(params.PRODUCT_VERSION != "NONE"){
                ProductVersion = params.PRODUCT_VERSION
              }
              else{
                ProductVersion = "0.0.${BUILD_NUMBER}"
              }

              // Set the build time limit from the parameter
              if (params.TIME_LIMIT){
                STEP_TIME_LIMIT = params.TIME_LIMIT
              }
            }
          }
        }

        stage('Clone Source') {
          steps {
            script {
              // Get some code from a GitHub repository
              def scmVars = checkout scm
              echo "scmVars: ${scmVars}"
              GIT_BRANCH = scmVars.GIT_BRANCH.replace('/','-')
              GIT_COMMIT = scmVars.GIT_COMMIT.substring(0,7)

              // Read package.json for the image name and app version.
              def packageJson = readJSON file: 'web-server/package.json'
              APP_VERSION = packageJson['version']
              IMAGE_NAME = packageJson['name']
              IMAGE_TAG = "${GIT_BRANCH}-${GIT_COMMIT}"
              REGISTRY_URI = "${AF_REGISTRY}/${IMAGE_NAME}"
              if (scmVars["GIT_PREVIOUS_SUCCESSFUL_COMMIT"]){
                GIT_PREVIOUS_COMMIT = scmVars["GIT_PREVIOUS_SUCCESSFUL_COMMIT"].substring(0,7)
              }
              if (GIT_BRANCH == MASTER_BRANCH_NAME){
                rtBuildInfo (captureEnv: true, maxBuilds: AF_MAX_MASTER_BUILDS, deleteBuildArtifacts: false,)
              }
              else {
                rtBuildInfo (captureEnv: true, maxBuilds: AF_MAX_BUILDS, deleteBuildArtifacts: true,)
              }
              echo """
              ProductVersion      = ${ProductVersion}
              APP_VERSION         = ${APP_VERSION}
              IMAGE_NAME          = ${IMAGE_NAME}
              IMAGE_TAG           = ${IMAGE_TAG}
              GIT_COMMIT          = ${GIT_COMMIT}
              GIT_PREVIOUS_COMMIT = ${GIT_PREVIOUS_COMMIT}
              Container Registry  = ${REGISTRY_URI}
              """
            }
          }
        }

        ///////////////////
        // Application Code
        ///////////////////
        stage('Application Code') {
          stages{
            stage('Dependencies') {
              steps {
                dir('web-client') {
                  sshagent(['bitbucket-act-project-readonly']) {
                    sh 'ssh-add -l'
                    sh 'cat ~/.ssh/known_hosts'
                    sh 'ssh git@bitbucket.org'
                    sh 'npm ci'
                  }
                }
              }
            }
            stage('Build') {
              steps {
                dir('web-client') {
                  sh "npm run updateBuild -- ${BUILD_NUMBER}-${GIT_COMMIT}"
                  sh 'npm run build -- --prod'
                }
              }
            }
            stage('Lint + Unit Tests') {
              steps {
                dir('web-client') {
                  sh 'npm run lint'
                  // This requires chrome, further work tbd
                  // sh 'npm run test'
                }
              }
            }
            stage('NPM Audit') {
              steps {
                dir('web-client') {
                  sh 'npm run audit --production'
                }
              }
            }
          }
        }
        ///////////////////
        // API Code
        ///////////////////
        stage('API Code') {
          stages{
            stage('Dependencies') {
              steps {
                dir('web-server') {
                  sshagent(['bitbucket-act-project-readonly']) {
                    sh 'npm ci'
                  }
                }
              }
            }
            stage('Build') {
              steps {
                dir('web-server') {
                  sh 'npm run build'
                }
              }
            }
            stage('Lint + Unit Tests') {
              steps {
                dir('web-server') {
                  sh 'npm run lint'
                  sh 'cp .env.example .env'
                  sh 'npm run test'
                }
              }
            }
            stage('NPM Audit') {
              steps {
                dir('web-server') {
                  sh 'npm run audit --production'
                }
              }
            }
          }
        }
        stage('Static Code Analysis') {
          options { timeout(time: STEP_TIME_LIMIT) }
          when { 
            // Don't resubmit to FOD if the code hasn't changed
            // Don't submit unless PR into master
            allOf{
              expression { RUN_SECURITY_SCAN }
              changeRequest target: MASTER_BRANCH_NAME
            }
          }
          steps {
            container('java'){
              withCredentials([string(credentialsId: BSI_SECRET_ID, variable: 'FOD_BSI'), usernamePassword(credentialsId: API_SECRET_ID, passwordVariable: 'FOD_API_SEC', usernameVariable: 'FOD_API_KEY')]) {
                // Get a clean copy of the repo
                sh "git archive --format=zip --output=source.zip origin/${GIT_BRANCH}"
                sh "unzip -d fortify source.zip"
                // Remove any pattern specified in .fodignore
                sh '''for i in $(cat .fodignore); do find ./fortify/ -path "./fortify/$i" -print0 | xargs -0 rm -rfv; done;'''
                // Create zip for uploading to FoD
                sh '''zip -r fod.zip fortify'''
                // Download the latest FoD Upload jar
                sh 'curl -Lo FodUpload.jar https://github.com/fod-dev/fod-uploader-java/releases/download/v4.0.4/FodUpload.jar'
                // Upload the cleaned zip, and wait for results
                sh """java -jar FodUpload.jar -zipLocation fod.zip -apiCredentials "${FOD_API_KEY}" "${FOD_API_SEC}" -bsiToken ${FOD_BSI} -entitlementPreferenceType SingleScanOnly -pollingInterval 1 -scanPreferenceId Express -auditPreferenceId Automated"""
              }
            }
          }
        }
        stage('Build EXE') {
          steps {
            dir('web-server') {
              sh 'cp -rf ../web-client/dist/web-client dist/'
              sh 'cp -rf ../help dist/web-client'
              sh "npm run pkgci -- -o orcapi-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe"
              archiveArtifacts artifacts: "orcapi-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe", onlyIfSuccessful: true
            }
          }
        }
        stage('Build installer') {
          steps {
            rtDownload(
              serverId: 'artifactory.dev.actsw.net',
              spec: """{
                "files": [
                  {
                      "pattern": "windows-installers/ddagent/6.18.0/ddagent-cli-6.18.0.msi",
                      "target": "./installer/"
                    }
                ]
              }"""
            )
            dir('installer') {
              sh "cp -v '../web-server/orcapi-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe' ORCommand.exe"
              container('wix'){
                withCredentials([certificate(credentialsId: CODESIGN_P12, keystoreVariable: 'CODESIGN_CERT', passwordVariable: 'CODESIGN_CERT_PASS')]) {
                  sh "candle -ext WixFirewallExtension -ext WixUtilExtension -dProductVersion='${ProductVersion}' ORCommand.wxs"
                  sh 'light -ext WixFirewallExtension -ext WixUtilExtension -sval ORCommand.wixobj'
                  sh "candle -ext WixNetFxExtension -ext WixBalExtension -ext WixUtilExtension -dProductVersion='${ProductVersion}' ORCommandBundle.wxs"
                  sh 'light -ext WixNetFxExtension -ext WixBalExtension -ext WixUtilExtension -sval ORCommandBundle.wixobj'
                  sh "cp ORCommand.msi ORCommand-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.msi"
                  // Detach the engine from ORCommandBundle.exe
                  sh "insignia -ib ORCommandBundle.exe -o ORCommandBundleEngine.exe"
                  // Sign the engine with our certificate
                  sh "osslsigncode sign -pkcs12 ${CODESIGN_CERT} -pass ${CODESIGN_CERT_PASS} -n 'ORCommand Bundle Executable' -i http://www.arthrex.com -t http://timestamp.verisign.com/scripts/timstamp.dll -in ORCommandBundleEngine.exe -out ORCommandBundleEngine-signed.exe"
                  sh "mv ORCommandBundleEngine-signed.exe ORCommandBundleEngine.exe"
                  // Re-attach the signed engine to the bundle
                  sh "insignia -ab ORCommandBundleEngine.exe ORCommandBundle.exe -o ORCommandBundle.exe"
                  // Sign the Installer bundle with our certificate
                  sh "osslsigncode sign -pkcs12 ${CODESIGN_CERT} -pass ${CODESIGN_CERT_PASS} -n 'ORCommand Bundle Executable' -i http://www.arthrex.com -t http://timestamp.verisign.com/scripts/timstamp.dll -in ORCommandBundle.exe -out ORCommandBundle-signed.exe"
                  sh "mv ORCommandBundle-signed.exe ORCommandBundle-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe"
                  sh "osslsigncode verify -in ORCommandBundle-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe"
                }
              }
              archiveArtifacts artifacts: "ORCommand-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.msi", onlyIfSuccessful: true
              archiveArtifacts artifacts: "ORCommandBundle-${GIT_BRANCH}-${BUILD_NUMBER}-${GIT_COMMIT}.exe", onlyIfSuccessful: true
              rtUpload(
                serverId: 'artifactory.dev.actsw.net',
                failNoOp: true,
                spec: """{
                  "files": [
                      {
                          "pattern": "ORCommand-*.msi",
                          "target": "or-command/${GIT_BRANCH}/"
                      },
                      {
                          "pattern": "ORCommandBundle-*.exe",
                          "target": "or-command/${GIT_BRANCH}/"
                      }
                  ]}"""
              )
              rtPublishBuildInfo (
                serverId: 'artifactory.dev.actsw.net'
              )
            }
          }
        }
      }
    }
  }
}

