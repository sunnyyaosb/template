import * as dotenv from 'dotenv'

dotenv.config()

export default {
  NODE_ENV: process.env.NODE_ENV || 'production',
  APP_NAME: process.env.APP_NAME || 'app',
  SERVER_PORT: process.env.SERVER_PORT || 80,
  SERVER_SSL_PORT: process.env.SERVER_SSL_PORT || 443,
  SECURE_SESSION: process.env.SECURE_SESSION || 'true',
}
