import { Request } from 'express'
// @ts-ignore
import passport from 'passport'
import * as LocalStrategy from 'passport-local'

passport.serializeUser((user, next) => {
  next(null, user)
})
passport.deserializeUser((obj, next) => {
  next(null, obj)
})

// local for on-prem auth
// @ts-ignore
// @ts-ignore
passport.use(
  'local',
  new LocalStrategy.Strategy(
    {
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
    },
    async (req: Request, username: string, password: string, done: any) => {
      // try {
      //   const user: User = await Container.get<UserService>(
      //     UserService
      //   ).getUserIfAuthenticated(username, password)
      //   if (user) {
      //     const parsedUser: IUser = {
      //       email: user.email,
      //       title: user.title,
      //       firstName: user.nameFirst,
      //       lastName: user.nameLast,
      //       groups: user.groups,
      //       userType: user.userType,
      //       id: user.id,
      //       username: user.username
      //     }
      //     return done(null, parsedUser)
      //   }
      //   return done(null, false)
      // } catch (err) {
      //   done(err, false)
      // }

      return done(null, {
              email: 'user.email',
              title: 'user.title',
              firstName: 'user.nameFirst',
              lastName: 'user.nameLast',
              groups: 'user.groups',
              userType: 'user.userType',
              id: 'user.id',
              username: 'user.username'
            })
    }
  )
)

export default passport
