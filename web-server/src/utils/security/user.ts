import { Request } from 'express'
import _ from 'lodash'

export default interface IUser {
  email: string
  id: string
  title: string
  username: string
  firstName: string
  lastName: string
  groups: string[]
  userType?: string
}

export function getUser(req: Request): IUser {
  // get from user session
  const sessionUser: IUser = _.get(req, 'user', null) as IUser
  if (sessionUser) {
    return sessionUser
  }

  // return empty user if none found
  const emptyUser: IUser = {
    email: '',
    firstName: '',
    lastName: '',
    username: '',
    groups: [],
    id: '',
    userType: '',
    title: ''
  }
  return emptyUser
}
