import graphqlHTTP from 'express-graphql'
import {buildSchema} from 'type-graphql'
import {Container} from 'typedi'
import rbac from './config/rbac.json'
import UserResolver from './user/resolver'

export const resolvers = [
  UserResolver
]

// @ts-ignore
const api = graphqlHTTP(async (request, response, graphQLParams) => {
  Container.set('rbac', rbac)
  const schema = await buildSchema({
    container: Container,
    resolvers
  })
  return {
    schema,
    graphiql: true,
    formatError: response
  }
})

export default api
