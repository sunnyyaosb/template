import * as express from 'express'
import env from '../utils/env'

const router = express.Router()

router.get('/', (req, res, next) => {
  if (env.NODE_ENV === 'development') {
    res.render('dev', {
      title: 'dev'
    })
  } else {
    res.status(403).send('Forbidden')
  }
})

export default router
