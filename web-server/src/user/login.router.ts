import * as express from 'express'

const router = express.Router()

router.post('/', async (req, res, next) => {
  await login(req, res, next)
})

export async function login(req, res, next) {
  const { username, password } = req.body

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: 'username and password are required' })
  }
  // if (await Container.get<UserService>(UserService).IsLdapUser(req)) {
  //   passport.authenticate('ldap', (err, user, info) => {
  //     if (err) {
  //       return next(err)
  //     }
  //     if (!user) {
  //       return res.status(401).json({ success: false, message: info.message })
  //     }
  //     req.logIn(user, error => {
  //       if (error) {
  //         return next(error)
  //       }

  return res.status(200).json({
          success: true,
          message: 'authentication succeeded',
          // user
        })
  //     })
  //     return res.send(user)
  //   })(req, res, next)
  // } else {
  //   passport.authenticate('local', (err, user) => {
  //     if (err) {
  //       return next(err)
  //     }
  //     if (!user) {
  //       return res
  //         .status(401)
  //         .json({ success: false, message: 'authentication failed' })
  //     }
  //     req.logIn(user, error => {
  //       if (error) {
  //         return next(error)
  //       }
  //       return res.status(200).json({
  //         success: true,
  //         message: 'authentication succeeded',
  //         user
  //       })
  //     })
  //   })(req, res, next)
  // }
}

export default router
