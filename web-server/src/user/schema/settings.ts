import { Field, ObjectType } from 'type-graphql'

@ObjectType()
export default class Settings {
  @Field(type => [String], { nullable: true })
  public rooms?: string[]
}
