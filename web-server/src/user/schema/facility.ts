import { Field, ObjectType } from 'type-graphql'

@ObjectType()
export default class UserFacility {
  @Field({ nullable: true })
  public default: boolean

  @Field({ nullable: true })
  public facilityName: string

  @Field({ nullable: true })
  public facilityAddress: string

  @Field({ nullable: true })
  public facilityPhone: string

  @Field({ nullable: true })
  public facilityWebsite: string

}
