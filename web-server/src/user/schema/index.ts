import { Field, ID, ObjectType } from 'type-graphql'
import Settings from './settings'

@ObjectType()
export default class User {
  @Field(type => ID)
  public id: string

  @Field({ nullable: true })
  public email: string

  @Field({ nullable: true })
  public username: string

  @Field({ nullable: true })
  public nameFirst: string

  @Field({ nullable: true })
  public title: string

  @Field({ nullable: true })
  public nameLast: string

  @Field({ nullable: true })
  public phone?: string

  @Field({ nullable: true })
  public status?: string

  @Field({ nullable: true })
  public userType: string

  @Field({ nullable: true })
  public onboarded: boolean

  @Field({ nullable: true })
  public deleted: boolean

  @Field({ nullable: true })
  public canDelete: boolean

  @Field(type => [String], { nullable: true })
  public groups?: string[]

  @Field(type => Settings, { nullable: true })
  public settings: Settings

  public hash: string

  public salt: string
}
