import { Field, InputType } from 'type-graphql'
import Settings from '../../user/schema/settings'

@InputType()
export default class SettingsInput implements Partial<Settings> {
  @Field(type => [String], { nullable: true })
  public rooms?: string[]
}
