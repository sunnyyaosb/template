import { Field, InputType } from 'type-graphql'
import Facility from '../../user/schema/facility'

@InputType()
export default class UserFacilityInput implements Partial<Facility> {
  @Field(() => Boolean, { nullable: true })
  public default: boolean

  @Field(() => String, { nullable: true })
  public facilityName: string

  @Field(() => String, { nullable: true })
  public facilityAddress: string

  @Field(() => String, { nullable: true })
  public facilityPhone: string

  @Field(() => String, { nullable: true })
  public facilityWebsite: string
}
