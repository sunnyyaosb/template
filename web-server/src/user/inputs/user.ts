import { Field, InputType } from 'type-graphql'
import User from '../../user/schema'
import SettingsInput from './settings'

@InputType()
export default class UserInput implements Partial<User> {
  @Field(() => String, { nullable: true })
  public email?: string

  @Field(() => String, { nullable: true })
  public username?: string

  @Field(() => String, { nullable: true })
  public title?: string

  @Field(() => String, { nullable: true })
  public nameFirst?: string

  @Field(() => String, { nullable: true })
  public nameLast?: string

  @Field(() => String, { nullable: true })
  public phone?: string

  @Field(() => String, { nullable: true })
  public status?: string

  @Field(() => String, { nullable: true })
  public userType?: string

  @Field(() => Boolean, { nullable: true })
  public onboarded?: boolean

  @Field(() => Boolean, { nullable: true })
  public deleted?: boolean

  @Field(() => Boolean, { nullable: true })
  public canDelete?: boolean

  @Field({ nullable: true })
  public password?: string

  @Field(type => [String], { nullable: true })
  public groups?: string[]

  @Field(() => SettingsInput, { nullable: true })
  public settings?: SettingsInput
}
