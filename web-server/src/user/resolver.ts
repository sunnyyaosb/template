import {Arg, Ctx, Query, Resolver} from 'type-graphql'
import User from './schema'

@Resolver(of => User)
export default class UserResolver {
  constructor(
    // private readonly userService: UserService
    // tslint:disable-next-line:no-empty
  ) {}

  @Query(() => User, { nullable: true })
  public async userById(@Arg('id') id: string, @Ctx() req: Request) {
    return {
        username: 'username',
        nameFirst: 'nameFirst',
        nameLast: 'nameLast'
    }
  }

  // @UseMiddleware(PermissionsGuard('read', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Query(() => [User])
  // public async allUsers() {
  //   return await this.userService.getAll()
  // }
  //
  // @UseMiddleware(PermissionsGuard('read', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Query(() => User)
  // public async userByEmail(@Arg('email') email: string, @Ctx() req: Request) {
  //   return await this.userService.userByEmail(email)
  // }
  //
  // @UseMiddleware(PermissionsGuard('read', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Query(() => User)
  // public async userByUsername(
  //   @Arg('username') username: string,
  //   @Ctx() req: Request
  // ) {
  //   return await this.userService.userByUsername(username)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public async createUser(@Arg('data') data: UserInput, @Ctx() req: Request) {
  //   return await this.userService.createUser(data)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public async updateUser(
  //   @Arg('data') data: UserInput,
  //   @(Arg('id')!) id: string,
  //   @Ctx() req: Request
  // ) {
  //   return await this.userService.updateUser(id, data)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public async deleteUser(@(Arg('id')!) id: string, @Ctx() req: Request) {
  //   return await this.userService.deleteUser(id)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public async assignRoomsToUser(
  //   @Arg('data') data: SettingsInput,
  //   @(Arg('id')!) id: string,
  //   @Ctx() req: Request
  // ) {
  //   return await this.userService.assignRoomsToUser(id, data.rooms)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public updateUserByEmail(
  //   @Arg('data') data: UserInput,
  //   @(Arg('email')!) email: string,
  //   @Ctx() req: Request
  // ) {
  //   return this.userService.updateUserByEmail(email, data)
  // }
  //
  // @UseMiddleware(PermissionsGuard('write', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public updateUserByUsername(
  //   @Arg('data') data: UserInput,
  //   @(Arg('username')!) username: string,
  //   @Ctx() req: Request
  // ) {
  //   return this.userService.updateUserByUsername(username, data)
  // }
  //
  // @UseMiddleware(PermissionsGuard('writeself', 'usermanagement'))
  // @UseMiddleware(LogAction())
  // @Mutation(() => User)
  // public updateCurrentUser(@Arg('data') data: UserInput, @Ctx() req: Request) {
  //   return this.userService.updateUserByUsername(getUser(req).username, data)
  // }
}
