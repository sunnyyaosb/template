// DO NOT move this, default.init() needs to be the first thing
// that happens when the backend starts - it'll check files and
// create defaults if needed

// import getFQDN from 'get-fqdn'
import * as http from 'http'
import app from './app'
import env from './utils/env'

// getFQDN()
//  .then(fqdn => {
    // tslint:disable-next-line:no-console
//    console.log(`Starting up on: ${fqdn}`)

    // Create a self signed cert if no cert is present
    // const server_options = {
    //   secureProtocol: 'TLSv1_2_method',
    //   key: fs.readFileSync("SERVER_KEY"),
    //   cert: fs.readFileSync(env.SERVER_CERT)
    // }

http.createServer(app).listen(env.SERVER_PORT, () => {
      // tslint:disable-next-line:no-console
      console.log(`Server started on port ${env.SERVER_PORT}`)
    })

    // https.createServer(server_options, app).listen(env.SERVER_SSL_PORT, () => {
    //   logger.info(`Server started on port ${env.SERVER_SSL_PORT}`)
    // })
  // })
  // .catch(reason => {
  //   // tslint:disable-next-line:no-console
  //   console.log(reason)
  // })
