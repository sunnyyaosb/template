import { resolverHasPermissions } from 'synergy-auth'
import { MiddlewareFn } from 'type-graphql'
import Container from 'typedi'
import env from '../utils/env'

export function PermissionsGuard(
  action: string,
  resource: string
): MiddlewareFn {
  return async ({ context }, next) => {
    if (env.NODE_ENV !== 'development') {
      await resolverHasPermissions(
        context,
        action,
        resource,
        Container.get('rbac')
      )
    }
    return next()
  }
}
