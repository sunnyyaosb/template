export class NotFoundError extends TypeError {
  constructor(message: string) {
    super(message)
  }
}
