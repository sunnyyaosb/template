
import bodyParser from 'body-parser'
import cors from 'cors'
import express, {NextFunction, Request, Response} from 'express'
import correlator from 'express-correlation-id'
import fileUpload from 'express-fileupload'
import session from 'express-session'
import helmet from 'helmet'
import memorystore from 'memorystore'
import path from 'path'
import 'reflect-metadata'
import {isAuthenticated} from 'synergy-auth'
import api from './api'
import devRouter from './dev/dev.router'
import loginRouter from './user/login.router'
import env from './utils/env'
import passport from './utils/security/passport'

const app: express.Application = express()

/**
 * session control
 */
const secureSession = env.SECURE_SESSION === 'true'
const MemoryStore = memorystore(session)
const expressSession = session({
  resave: false,
  saveUninitialized: false,
  secret: 'my_secret',
  store: new MemoryStore({
    checkPeriod: 86400000 // prune expired entries every 24h
  }),
  cookie: { secure: secureSession }
})
app.use(expressSession)

/**
 * cors middleware
 */
app.use(cors())

// passport setup
app.use(passport.initialize())
app.use(passport.session())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(helmet())
app.use(correlator())

/**
 * ejs views
 */
app.set('views', path.join(__dirname, '../views'))
app.set('view engine', 'ejs')

// set up a route to redirect http to https
app.use((req, res, next) => {
  res.removeHeader('X-Frame-Options')
  // if (req.secure) {
  //   if (!req.isAuthenticated) {
  //     res.redirect(`https://${req.headers.host}/login`)
  //   } else if (req.url === '/') {
  //     res.redirect(`https://${req.headers.host}/orlistcomponent`)
  //   } else {
  //     next()
  //   }
  // } else {
  //   let host = req.headers.host
  //   if (host.indexOf(':') > 0) {
  //     host = host.substring(0, host.indexOf(':'))
  //   }
  //   res.redirect(`https://${host}:${env.SERVER_SSL_PORT}${req.url}`)
  // }
  next()
})

/**
 * health check endpoint
 */
app.get('/hc', (req, res) => {
  res.send('Health check successful.')
})

app.use(fileUpload())

/**
 * graphql endpoint
 */
if (env.NODE_ENV !== 'development') {
  app.use('/graphql', isAuthenticated, api)
} else {
  app.use('/graphql', api)
}

app.get('/', (req, res) => {
  if (req.isAuthenticated()) {
    // res.redirect('/home')
    res.json({ message: 'Session is alive' })
  } else {
    res.send(
      `Please <a href="/login">login</a> or close your browser to end session`
    )
  }
})

app.use('/dev', devRouter)

/**
 * login endpoints
 */
app.use('/login', loginRouter)

/**
 * logout endpoint
 */
app.get('/logout', (req: Request, res: Response) => {
  req.logout()
  if (req.session) {
    req.session.destroy(err => undefined)
  }
  // res.redirect('/')
  res.json({ message: 'Logged out' })
})

app.get('/', (req, res) => {
  if (req.isAuthenticated()) {
    // res.redirect('/home')
    res.json({ message: 'Session is alive' })
  } else {
    res.send(
      `Please <a href="/login">login</a> or close your browser to end session`
    )
  }
})

app.use((req, res, next) => {
  res.status(404)
  return next('Not found')
})

// error handler to return error messages
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  // if (err.message) {
  //   res.json({ success: false, message: err.message, stack: err.stack })
  // } else {
    res.json({ success: false, message: err })
  // }
})

app._router.stack.forEach(r => {
  if (r.route && r.route.path) {
    // tslint:disable-next-line:no-console
    console.log((r.route.methods.get ? 'GET' : 'POST') + ':' + r.route.path)
  }
})
export default app
