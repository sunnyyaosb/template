module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      diagnostics: {
        warnOnly: true
      }
    }
  },
  setupFiles: ['<rootDir>/tests/helpers/setup.ts'],
  collectCoverage: true,
  coverageReporters: ['lcov', 'html']
}
