# ENVIRONMENT

Before running the application, we need to set the environment we're going to run. A `.env.example` is provided. It must be copied into a new `.env` file, with the default values replaced as necessary.

# COUCHBASE

We need `couchbase` setup before running the application.

It can be installed locally and a bucket can be created manually. That bucket's name should be set in the `.env` file, along with `localhost` as the couchbase host. There should also be at least one index on the bucket to allow for queries.

Or we could use the `couchbase` image for docker. If `docker` and `docker-compose` are installed, we just need to run `docker-compose up -d`.

# RUNNING NATIVELY

## PRE-REQUIREMENTS

We'll use `nvm` as it's the most popular option to manage node versions. That way we can lock the version of node we all use and ensure that doesn't cause errors between environments.

## INSTALLING

Install the node version and packages.

    nvm install
    nvm use
    npm install

## RUNNING

    npm run dev

## LINT

    npx prettier --write src/index.ts

## TEST

    npm run test

# RUNNING IN DOCKER

We're going to do the deployments in docker, but any developer who has it installed could use it locally too.

## PRE-REQUIREMENTS

    docker
    docker-compose

## RUNNING

    # LOCALLY
    docker-compose -f docker-compose.yaml -f docker-compose.dep.yaml up -d

## LINT

    docker-compose -f docker-compose.dep.yaml run --rm app npx prettier --write src/index.ts

## TEST

    docker-compose -f docker-compose.dep.yaml run --rm app npm run test
