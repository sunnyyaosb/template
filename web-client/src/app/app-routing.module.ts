// tslint:disable-next-line: max-line-length
import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {AuthGuard} from './user/auth.guard'
import {RbacGuard} from './user/rbac.guard'
import {MainComponent} from './components/main/main.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  // {
  //   path: 'login',
  //   // component: LoginComponent
  // },
  {
    path: 'main',
    canActivateChild: [AuthGuard, RbacGuard],
    data: {
      breadcrumb: 'Dashboard'
    },
    children: [
      {
        path: '',
        component: MainComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
