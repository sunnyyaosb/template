import gql from 'graphql-tag'
import { UserAttributes } from '../fragments/UserAttributes'

export const UserByIdQuery = gql`
  query userById($id: String!) {
    userById(id: $id) {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`
export const allUsersQuery = gql`
  query allUsers {
    allUsers {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`
