import gql from 'graphql-tag'

export const UpdateUserAttributes = gql`
  fragment UpdateUserAttributes on User {
    __typename
    id
    email
    userType
    username
    password
    title
    nameFirst
    nameLast
    userType
    deleted
    status
    groups
    settings {
      rooms
    }
  }
`
