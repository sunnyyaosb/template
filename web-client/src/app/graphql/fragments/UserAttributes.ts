import gql from 'graphql-tag'

export const UserAttributes = gql`
  fragment UserAttributes on User {
    __typename
    id
    email
    userType
    username
    title
    nameFirst
    nameLast
    status
    groups
    settings {
      rooms
    }
  }
`
