import gql from 'graphql-tag'
import { UserAttributes } from './../fragments/UserAttributes'

export const UpdateUserByEmail = gql`
  mutation updateUserByEmail($email: String!, $data: UserInput!) {
    userByEmail(email: $email, data: $data) {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`

export const UpdateUserById = gql`
  mutation updateUser($id: String!, $data: UserInput!) {
    updateUser(id: $id, data: $data) {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`
export const UpdateCurrentUser = gql`
  mutation updateCurrentUser($data: UserInput!) {
    updateCurrentUser(data: $data) {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`
