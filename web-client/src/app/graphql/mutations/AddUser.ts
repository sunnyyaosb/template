import gql from 'graphql-tag'
import { UserAttributes } from './../fragments/UserAttributes'

export const CreateUser = gql`
  mutation CreateUser($data: UserInput!) {
    createUser(data: $data) {
      ...UserAttributes
    }
  }
  ${UserAttributes}
`
