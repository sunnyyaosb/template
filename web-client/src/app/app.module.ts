
import { OverlayContainer } from '@angular/cdk/overlay'
import { CdkTableModule } from '@angular/cdk/table'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormField,
  MatFormFieldModule,
  MatIconModule,
  MatInput,
  MatInputModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import 'hammerjs'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import {AlertDialogComponent} from './shared/components/alert-dialog/alert-dialog.component'
import {MainComponent} from './components/main/main.component'

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AlertDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatSortModule,
    FormsModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule
  ],
  entryComponents: [
    AlertDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    AppComponent,
    MainComponent,
    MatSortModule,
    FormsModule,
    MatFormField,
    MatInput,
    MatDialogModule,
    MatSelectModule,
    MatTableModule
  ]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer
      .getContainerElement()
      .classList.add('angular-material-ads-app-theme')
  }
}
