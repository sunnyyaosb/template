export interface SettingsInput {
  rooms?: string[]
}

export interface UserInput {
  id?: string
  email?: string
  username?: string
  title?: string
  nameFirst?: string
  nameLast?: string
  password?: string
  status?: string
  userType?: string
  groups?: string[]
  deleted?: boolean
  facility?: UserFacilityInput[]
  settings?: SettingsInput
  readonly fullName?: string
}

export class User {
  id?: string
  email?: string
  username?: string
  title?: string
  nameFirst?: string
  nameLast?: string
  password?: string
  status?: string
  userType?: string
  groups?: string[]
  deleted?: boolean
  settings?: SettingsInput
  facility?: UserFacilityInput[]
  readonly fullName?: string

  constructor() {
    this.groups = ['user']
    this.userType = 'local'
    this.title = ''
  }
}

export class UserUIModel {
  user: User
  selected = false
  innerrole: string
  roomCount = 0

  constructor(user: User) {
    this.user = user
    if (user.settings && user.settings.rooms) {
      this.roomCount = user.settings.rooms.length
    }
    if (user.groups && user.groups.length) {
      this.group = user.groups[0]
    }
  }

  set group(value: string) {
    this.innerrole = value
    this.user.groups = [value]
  }

  get group(): string {
    return this.innerrole
  }

  get fullName(): string {
    let fullName = this.user.nameLast + ', ' + this.user.nameFirst
    if (this.user.title) {
      fullName = this.user.title + ' ' + fullName
    }
    return fullName
  }
}

export interface UserFacilityInput {
  default: boolean
  facilityName: string
  facilityAddress: string
  facilityPhone: string
  facilityWebsite: string
  facilityLogoContentId: string
}
