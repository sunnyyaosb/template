function compareStrings(first: string, second: string): number {
  if (!first) {
    first = ''
  }
  return first.localeCompare(second)
}

const flatMap = (a, cb) => [].concat(...a.map(cb))

function compareBooleans(first: boolean, second: boolean): number {
  return Number(first) - Number(second)
}

function compareDateStrings(first: string, second: string): number {
  return new Date(first).getTime() - new Date(second).getTime()
}

export { compareDateStrings, compareBooleans, compareStrings, flatMap }
