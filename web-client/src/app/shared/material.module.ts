import { CdkTableModule } from '@angular/cdk/table'
import { NgModule } from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// tslint:disable-next-line: max-line-length
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatDialogModule,
  MatDividerModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatOptionModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  imports: [
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatTableModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    CdkTableModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatButtonToggleModule,
    FormsModule
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    CdkTableModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatButtonToggleModule,
    FormsModule
  ]
})
export class MaterialModule {}
