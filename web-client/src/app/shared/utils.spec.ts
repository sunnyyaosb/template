import * as shared from './utils'

describe('shared/utils', () => {
  describe('compareStrings', () => {
    it('returns 1 when first is bigger', () => {
      expect(shared.compareStrings('zample', 'arguments')).toBe(1)
      expect(shared.compareStrings('zample', 'Arguments')).toBe(1)
      expect(shared.compareStrings('Zample', 'arguments')).toBe(1)
      expect(shared.compareStrings('Zample', 'zample')).toBe(1)
      expect(shared.compareStrings('Zample', 'argument')).toBe(1)
    })

    it('returns 0 when first is equal', () => {
      expect(shared.compareStrings('argument', 'argument')).toBe(0)
    })

    it('returns -1 when first is smaller', () => {
      expect(shared.compareStrings('argument', 'zample')).toBe(-1)
      expect(shared.compareStrings('arguments', 'Zample')).toBe(-1)
      expect(shared.compareStrings('Arguments', 'zample')).toBe(-1)
      expect(shared.compareStrings('zample', 'Zample')).toBe(-1)
      expect(shared.compareStrings('Argument', 'zample')).toBe(-1)
    })
  })

  describe('compareBooleans', () => {
    it('returns 1 when first is bigger', () => {
      expect(shared.compareBooleans(true, false)).toBe(1)
    })

    it('returns 0 when first is equal', () => {
      expect(shared.compareBooleans(true, true)).toBe(0)
    })

    it('returns -1 when first is smaller', () => {
      expect(shared.compareBooleans(false, true)).toBe(-1)
    })
  })

  describe('compareDateStrings', () => {
    const previousDate = '2012-04-17T12:04:04.433Z'
    const laterDate = '2019-04-17T12:04:04.433Z'

    it('returns positive number when first is bigger', () => {
      expect(
        shared.compareDateStrings(laterDate, previousDate)
      ).toBeGreaterThan(0)
    })

    it('returns 0 when first is equal', () => {
      expect(shared.compareDateStrings(previousDate, previousDate)).toBe(0)
    })

    it('returns negative number when first is smaller', () => {
      expect(shared.compareDateStrings(previousDate, laterDate)).toBeLessThan(0)
    })
  })
})
