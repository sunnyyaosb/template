import {Injectable} from '@angular/core'
import {Apollo} from 'apollo-angular'
import {BehaviorSubject, Observable} from 'rxjs'
import {allUsersQuery} from '../graphql/queries/UserQuery'
import {User} from '../model/user-input'

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  userError: any
  userLoading = false

  constructor(private apollo: Apollo) {}

  getAllUsers(): Observable<User[]> {
    const users = new BehaviorSubject<User[]>([])
    this.apollo
      .watchQuery<{ allUsers: User[] }>({
        query: allUsersQuery
      })
      .valueChanges.subscribe(
        ({ loading, data, errors }) => {
          if (data && data.allUsers) {
            users.next(data.allUsers)
          }
          this.userLoading = loading
          this.userError = errors
        },
        error => (this.userError = error)
      )
    return users
  }
}
