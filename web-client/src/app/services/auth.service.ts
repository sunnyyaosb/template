import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'

export class LoginInput {
  username: string
  password: string
}

export class Login {
  username: string
  password: string
  // tslint:disable-next-line:variable-name
  login_type: string

  constructor(user: LoginInput) {
    this.username = user.username
    this.password = user.password
    this.login_type = 'local'
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url: string
  redirectUrl: string
  get isLoggedIn(): boolean {
    // return this.sessionService.getUserSession() !== null
    return true
  }
  get isAdmin(): boolean {
    // return this.sessionService.isAdmin()
    return true
  }
  constructor(
    private http: HttpClient,
    // private sessionService: SessionService
  ) {
    this.url = environment.APIUrl
  }

  authenticate(login: Login) {
    return this.http.post(this.url + '/login', login, {
      headers: this.setHeaders()
    })
  }

  logout() {
    return this.http.get(this.url + '/logout', {
      headers: this.setHeaders()
    })
  }

  setHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
}
