import { Injectable } from '@angular/core'
import { User } from '../model/user-input'

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  user: User
  userRole = 'user'
  // constructor(private store: StoreService) {}
  // setUserSession(user: User) {
  //   this.store.setCurrentUser(user)
  //   sessionStorage.setItem('user', JSON.stringify(user))
  //   this.userRole = 'user'
  //   if (user.groups.length) {
  //     this.userRole = user.groups[0]
  //   }
  //   sessionStorage.setItem('group', this.userRole)
  // }
  // getUserSession() {
  //   if (!this.user) {
  //     this.user = this.getCurrentUser()
  //     if (this.user) {
  //       this.setUserSession(this.user)
  //     }
  //   }
  //   return this.user
  // }
  //
  // getCurrentUser() {
  //   return JSON.parse(sessionStorage.getItem('user'))
  // }
  //
  // getCurrentUserRole() {
  //   return sessionStorage.getItem('group')
  // }
  //
  // isAdmin(): boolean {
  //   if (this.userRole) {
  //     return this.userRole === 'admin'
  //   } else {
  //     this.userRole = this.getCurrentUserRole()
  //   }
  //   return this.userRole === 'admin'
  // }
  //
  // endUserSession() {
  //   this.user = null
  //   this.store.resetUser()
  //   sessionStorage.removeItem('user')
  //   sessionStorage.removeItem('group')
  // }
}
