import { Component, HostListener } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'web-client'
  scrHeight: any
  toolbarVisible = true

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight
  }

  constructor() {
    this.getScreenSize()
    // Add global to window, assigning the value of window itself.
    // tslint:disable-next-line:align
    ;(window as any).global = window
    // Defining process shim
    // tslint:disable-next-line:align
    ;(window as any).process = undefined
  }
}
