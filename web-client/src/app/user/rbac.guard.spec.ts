import { HttpClientTestingModule } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { RouterModule } from '@angular/router'
import { ApolloModule } from 'apollo-angular'
import { GraphQLModule } from 'src/app/graphql.module'
import { RbacGuard } from './rbac.guard'

describe('RbacGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ApolloModule,
        GraphQLModule,
        RouterModule.forRoot([]),
        GraphQLModule,
        HttpClientTestingModule
      ],
      providers: [RbacGuard]
    })
  })

  it('should ...', inject([RbacGuard], (guard: RbacGuard) => {
    expect(guard).toBeTruthy()
  }))
})
