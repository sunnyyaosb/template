import { HttpClientTestingModule } from '@angular/common/http/testing'
import { inject, TestBed } from '@angular/core/testing'
import { RouterModule } from '@angular/router'
import { ApolloModule } from 'apollo-angular'
import { GraphQLModule } from 'src/app/graphql.module'
import { AuthGuard } from './auth.guard'

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ApolloModule,
        GraphQLModule,
        RouterModule.forRoot([]),
        GraphQLModule,
        HttpClientTestingModule
      ],
      providers: [AuthGuard]
    })
  })

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy()
  }))
})
