import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  // Router,
  RouterStateSnapshot
} from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class RbacGuard implements CanActivate, CanActivateChild {
  // currentUser: User

  // constructor(private router: Router, private store: StoreService) {
  //   this.store.getCurrentUser().subscribe(user => {
  //     this.currentUser = user.value
  //   })
  // }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const ROUTE_DATA_ACCESS_RIGHTS = 'access'

    while (route.firstChild) {
      route = route.firstChild
    }

    if (route.data.hasOwnProperty(ROUTE_DATA_ACCESS_RIGHTS)) {
      // const userRights: string[] = route.data[ROUTE_DATA_ACCESS_RIGHTS]

      const hasRight = false
      // userRights.forEach(value => {
      //   if (
      //     // this.currentUser.groups.findIndex(group => group === value) !== -1
      //   ) {
      //     hasRight = true
      //   }
      // })

      // if (!hasRight) {
      //   this.router.navigate(['/orlistcomponent'])
      // } else {
      return hasRight
      // }
    }

    return true
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.canActivate(route, state)
  }
}
