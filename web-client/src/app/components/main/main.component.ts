import {
  ChangeDetectorRef,
  Component,
  // NgZone,
  OnDestroy,
  OnInit
} from '@angular/core'
import { MatPaginatorIntl } from '@angular/material'

export class MatPaginatorHideLabels extends MatPaginatorIntl {
  itemsPerPageLabel = ''
  nextPageLabel = ''
  previousPageLabel = ''
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'main-component',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorHideLabels }]
})
export class MainComponent implements OnInit, OnDestroy {
  public state = ''
  pagination = true
  paginatorVisible = false

  constructor(
    // private route: ActivatedRoute,
    // private router: Router,
    private chgRef: ChangeDetectorRef,
    // private ngZone: NgZone,
  ) {
    this.chgRef.detach()
  }

  selectedPageSize = '-1'
  pageSize = 12
  pageNumber = 0

  // @ts-ignore
  gridColumnsPerCount: { [id: number]: number } = []
  gridColumns = 4
  maxWidth: any = '145vh'

  facilityName = 'OR Command'
  ngOnInit() {
    this.gridColumnsPerCount[1] = 1
    this.gridColumnsPerCount[2] = 2
    this.gridColumnsPerCount[4] = 2
    this.gridColumnsPerCount[8] = 4
    this.gridColumnsPerCount[9] = 3
    this.gridColumnsPerCount[12] = 4
    this.gridColumnsPerCount[16] = 4
    this.gridColumnsPerCount[48] = 4

    this.maxWidth = '145vh'
  }

  public ngOnDestroy(): void {
  }

  updateColumnsAndMaxWidth() {
    this.paginatorVisible = true
    this.chgRef.detectChanges()
  }
}
