import { HttpClientModule } from '@angular/common/http'
import { async, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { ApolloTestingModule } from 'apollo-angular/testing'

import { MaterialModule } from '../../shared/material.module'
import {MainComponent} from './main.component'

describe('MainComponent', () => {
  // tslint:disable-next-line:prefer-const
  let component: MainComponent

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainComponent,
      ],
      providers: [],
      imports: [
        HttpClientModule,
        ApolloTestingModule,
        MaterialModule,
        RouterTestingModule
      ]
    }).compileComponents()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
