import { NgModule } from '@angular/core'
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular'
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context'
import { environment } from 'src/environments/environment'

export function createApollo(httpLink: HttpLink) {
  const http = httpLink.create({ uri: environment.GRAPHQL_URI })
  const auth = setContext((_, { headers }) => {
    // return oktaAuth.getAccessToken().then(token => {
    return {}
    // });
  })

  return {
    link: auth.concat(http),
    cache: new InMemoryCache()
  }
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    }
  ]
})
export class GraphQLModule {}
