export function hostFactory() {
  return window.location.hostname
}

export const environment = {
  useSSL: true,
  production: true,
  APIUrl: 'https://' + hostFactory(),
  GRAPHQL_URI: 'https://' + hostFactory() + '/graphql',
  version: '{BUILD_VERSION}'
}
