// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export function hostFactory() {
  // return window.location.hostname
  return 'localhost'
}

export const environment = {
  // useSSL: true,
  // production: true,
  // APIUrl: 'https://' + hostFactory(),
  // GRAPHQL_URI: 'https://' + hostFactory() + '/graphql',

  useSSL: false,
  production: false,
  APIUrl: 'http://' + hostFactory() + ':80',
  GRAPHQL_URI: 'http://' + hostFactory() + ':80/graphql',
  version: '{BUILD_VERSION}'
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
